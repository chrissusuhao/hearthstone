﻿using UnityEngine;
using System.Collections;

public class Hero2Crystal : MonoBehaviour {

    public int usableName = 1;
    public int totalNumber = 1;
    public int maxNumber = 10;
    private UILabel label;

    void Awake()
    {
        label = GetComponent<UILabel>();
    }

    void Start()
    {
        GameController._instance.OnNewRound += OnNewRound;
    }

    void Update()
    {
        UpdateShow();
    }

    public void UpdateShow()
    {
        label.text = usableName + "/" + totalNumber;
    }

    public bool GetCrystal(int number)
    {
        if (usableName >= number)
        {
            usableName -= number;
            UpdateShow();
            return true;
        }
        else
        {
            return false;
        }

    }

    public void RefreshCrystalNumber()
    {
        if (totalNumber < maxNumber)
        {
            totalNumber++;
        }
        usableName = totalNumber;
        UpdateShow();
    }

    public void OnNewRound(string heroName)
    {
        if(heroName == "hero2")
        {
            if(GameController._instance.roundIndex >= 2)
            {
                RefreshCrystalNumber();
            }
        }
    }
}
