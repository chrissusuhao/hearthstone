﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MyCard : MonoBehaviour {

    public GameObject cardPrefab;
    public Transform card01;
    public Transform card02;

    private float xOffset;
    private List<GameObject> cards = new List<GameObject>();

    void Start()
    {
        xOffset = card02.position.x - card01.position.x;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            AddCard();
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            LoseCard();
        }
    }

    public void LoseCard()
    {
        int index = Random.Range(0, cards.Count);
        Destroy(cards[index]);
        cards.RemoveAt(index);

        UpdateShow();

       
    }

    public void UpdateShow()
    {
        for (int i = 0; i < cards.Count; i++)
        {
            Vector3 toPosition = card01.position + new Vector3(xOffset * i, 0, 0);
            iTween.MoveTo(cards[i], toPosition, 0.5f);
            cards[i].GetComponent<Card>().SetDepth(startDepth + i*2);
        }
    }

    public string[] cardNames;
    private readonly int startDepth = 1;

    public void AddCard(GameObject cardGo = null)
    {
        if(cardGo == null)
        {
            //如果方法没有传递过来卡牌的话，自己创建
            cardGo = NGUITools.AddChild(gameObject, cardPrefab);
            cardGo.GetComponent<UISprite>().spriteName = cardNames[Random.Range(0, cardNames.Length)];
        }
        cardGo.transform.parent = transform;
        cardGo.GetComponent<UISprite>().width = 56;
        cardGo.GetComponent<UISprite>().height = 150;
        cardGo.GetComponent<Card>().ResetShow();
        Vector3 toPosition = card01.position + new Vector3(xOffset * cards.Count, 0, 0);
        iTween.MoveTo(cardGo, toPosition, 1f);
        cards.Add(cardGo);
        cardGo.GetComponent<Card>().SetDepth(startDepth + (cards.Count-1)*2);
    }

    public void RemoveCard(GameObject go)
    {

    }
}
