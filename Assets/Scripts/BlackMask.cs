﻿using UnityEngine;
using System.Collections;

public class BlackMask : MonoBehaviour {

    public static BlackMask _instance;
    
    void Awake()
    {
        _instance = this;
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

}
