﻿using UnityEngine;
using System.Collections;
using System;

public enum GameState
{
    CardGenerating,
    PlayCard,
    End
}

public class GameController : MonoBehaviour {

    public static GameController _instance;

    public GameState gameState = GameState.CardGenerating;
    public float cycleTime;
    public MyCard myCard;
    public EnemyCard enemyCard;

    public int roundIndex = 0;
    public delegate void OnNewRoundEvent(string heroName);
    public event OnNewRoundEvent OnNewRound;

    private UISprite wickpopeSprite;
    private float timer = 0f;
    private float wickpopeLength;
    private string currentHeroName = "hero1";
    private CardGenerator cardGenerator;

    void Awake()
    {
        _instance = this;
        wickpopeSprite = transform.Find("wickpope").GetComponent<UISprite>();
        wickpopeLength = wickpopeSprite.width;
        wickpopeSprite.width = 0;
        cardGenerator = GetComponent<CardGenerator>();
        StartCoroutine(GenerateCardForHero1());
    }

    private IEnumerator GenerateCardForHero1()
    {
        //最开始发牌4张
        for(int i = 0; i < 4; i++)
        {
            GameObject cardGo =  cardGenerator.RandomGenerateCard();//调用方法随机生成一张卡牌
            yield return new WaitForSeconds(2f);
            myCard.AddCard(cardGo);
        }
        for (int i = 0; i < 4; i++)
        {
            GameObject cardGo = cardGenerator.RandomGenerateCard();//调用方法随机生成一张卡牌
            yield return new WaitForSeconds(2f);
            enemyCard.AddCard(cardGo);
        }
        gameState = GameState.PlayCard;
        timer = 0;
    }

    void Update()
    {
        if(gameState == GameState.PlayCard)
        {
            timer += Time.deltaTime;
            if(timer > cycleTime)
            {
                TransformPlayer();
            }
            else if(cycleTime - timer <= 15)
            {
                wickpopeSprite.width = (int)(((cycleTime - timer) / 15f) * wickpopeLength);
            }
        }
    }

    public void TransformPlayer()
    {
        timer = 0;
        if(currentHeroName == "hero1")
        {
            currentHeroName = "hero2";
        }
        else
        {
            currentHeroName = "hero1";
        }
        roundIndex++;

        OnNewRound(currentHeroName);

        if(roundIndex >= 2)
        {
            StartCoroutine(DealCard());
        }
    }

    IEnumerator DealCard()
    {
        gameState = GameState.CardGenerating;
        if(currentHeroName == "hero1")
        {
            for (int i = 0; i < 2; i++)
            {
                GameObject cardGo = cardGenerator.RandomGenerateCard();//调用方法随机生成一张卡牌
                yield return new WaitForSeconds(2f);
                myCard.AddCard(cardGo);
            }
        }
        else
        {
            for (int i = 0; i < 2; i++)
            {
                GameObject cardGo = cardGenerator.RandomGenerateCard();//调用方法随机生成一张卡牌
                yield return new WaitForSeconds(2f);
                enemyCard.AddCard(cardGo);
            }
        }
        gameState = GameState.PlayCard;
        timer = 0;
    }
}
