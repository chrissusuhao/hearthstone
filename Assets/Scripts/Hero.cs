﻿using UnityEngine;
using System.Collections;

public class Hero : MonoBehaviour {

    public int minHp = 0;
    public int maxHp = 30;

    protected UISprite sprite;
    private UILabel hpLabel;
    private int hpCount = 30;

    // Use this for initialization
    void Awake()
    {
        sprite = GetComponent<UISprite>();
        hpLabel = transform.Find("hp").GetComponent<UILabel>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            TakeDamage(Random.Range(1, 5));
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            PlusHp(Random.Range(1, 5));
        }
    }

    public void TakeDamage(int damage)
    {
        hpCount -= damage;
        hpLabel.text = hpCount + "";

        if (hpCount <= minHp)
        {

        }
    }

    public void PlusHp(int hp)
    {
        hpCount += hp;
        if (hpCount > maxHp)
        {
            hpCount = maxHp;
        }
        hpLabel.text = hpCount + "";
    }
}
