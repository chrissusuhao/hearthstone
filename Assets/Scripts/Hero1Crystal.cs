﻿using UnityEngine;
using System.Collections;

public class Hero1Crystal : MonoBehaviour {

    public int usableName = 1;
    public int totalNumber = 1;
    public int maxNumber;
    public UISprite[] crystals;
    private UILabel label;

    void Awake()
    {
        maxNumber = crystals.Length;
        label = GetComponent<UILabel>();
    }

    void Start()
    {
        GameController._instance.OnNewRound += OnNewRound;
    }

    void Update()
    {
        UpdateShow();
    }

    public void RefreshCrystalNumber()
    {
        if(totalNumber < maxNumber)
        {
            totalNumber++;
        }
        usableName = totalNumber;
        UpdateShow();
    }

    public bool GetCrystal(int number)
    {
        if(usableName >= number)
        {
            usableName -= number;
            UpdateShow();
            return true;
        }
        else
        {
            return false;
        }

    }

    void UpdateShow()
    {
        for(int i = totalNumber; i < maxNumber; i++)
        {
            crystals[i].gameObject.SetActive(false);
        }

        for(int i = 0; i < totalNumber; i++)
        {
            crystals[i].gameObject.SetActive(true);
        }

        for(int i = usableName; i < totalNumber; i++)
        {
            crystals[i].spriteName = "TextInlineImages_normal";
        }
        
        for(int i = 0; i < usableName; i++)
        {
            if(i == 9)
            {
                crystals[i].spriteName = "TextInlineImages_" + (i + 1);
            }
            crystals[i].spriteName = "TextInlineImages_0" + (i + 1);
        }
        label.text = usableName + "/" + totalNumber;
    }

    public void OnNewRound(string heroName)
    {
        if(heroName == "hero1")
        {
            RefreshCrystalNumber();
        }
    }

}
