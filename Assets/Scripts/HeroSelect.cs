﻿using UnityEngine;
using System.Collections;

public class HeroSelect : MonoBehaviour {

    private UISprite selectHeroImage;
    private UILabel selectHeroName;

    void Awake()
    {
        selectHeroImage = transform.parent.Find("hero0").GetComponent<UISprite>();
        selectHeroName = transform.parent.Find("hero_name").GetComponent<UILabel>();
    }

    void OnClick()
    {
        string heroName = gameObject.name;
        selectHeroImage.spriteName = heroName;
        selectHeroName.text = heroName;
    }
}
