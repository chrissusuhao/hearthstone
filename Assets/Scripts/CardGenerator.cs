﻿using UnityEngine;
using System.Collections;

public class CardGenerator : MonoBehaviour {
    public GameObject cardPrefab;
    public Transform fromCard;
    public Transform toCard;
    public string[] cardNames;

    public float transformTime = 2f;
    public int transformSpeed = 20;

    private bool isTransforming = false;
    private float timer = 0;
    private UISprite nowGenerateCard;


    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            RandomGenerateCard();
        }

        if (isTransforming)
        {
            timer += Time.deltaTime;
            int index = (int)(timer / (1f / transformSpeed));
            index %= cardNames.Length;
            nowGenerateCard.spriteName = cardNames[index];
            if (timer > transformTime)
            {
                //随机生成一个卡牌名字
                string cardName = cardNames[Random.Range(0, cardNames.Length)];
                nowGenerateCard.spriteName = cardName;
                nowGenerateCard.GetComponent<Card>().InitProperty();
                timer = 0;
                isTransforming = false;
            }
        }
    }

    public GameObject RandomGenerateCard()
    {
        GameObject go = NGUITools.AddChild(gameObject, cardPrefab);
        go.transform.position = fromCard.transform.position;
        nowGenerateCard = go.GetComponent<UISprite>();
        isTransforming = true;
        iTween.MoveTo(go, toCard.transform.position, 1f);
        return go;
    }
}
