﻿using UnityEngine;
using System.Collections;
using System;

public class StartMenu : MonoBehaviour {

    public MovieTexture movTexture;
    public bool isDrawMov = true;
    public bool isShowMessage = false;
    public TweenScale logoTweenScale;

    private bool isCanShowSelectRole = false;

    public TweenPosition selectRoleTween;

    public UISprite hero1;

    public string[] HeroName = { "吉安娜", "雷克萨", "乌瑟尔", "加尔鲁", "玛法里奥", "古尔丹", "萨尔", "安杜因", "瓦利拉" };

    // Use this for initialization
    void Start () {
        movTexture.loop = false;
        movTexture.Play();
        logoTweenScale.AddOnFinished(OnLogoTweenFinished);
	}

    private void OnLogoTweenFinished()
    {
        isCanShowSelectRole = true;
    }

    // Update is called once per frame
    void Update () {
        if (isDrawMov)
        {
            if (Input.GetMouseButton(0) && isShowMessage == false)
            {
                isShowMessage = true;
            }
            else if(Input.GetMouseButton(0) && isShowMessage == true)
            {
                StopMov();
            }
        }

        if(movTexture.isPlaying != isDrawMov)
        {
            StopMov();
        }

        if(isCanShowSelectRole && Input.GetMouseButtonDown(0))
        {
            isCanShowSelectRole = false;
            selectRoleTween.PlayForward();
        }
    }

    private void StopMov()
    {
        movTexture.Stop();
        isDrawMov = false;

        logoTweenScale.PlayForward();
    }



    void OnGUI()
    {
        if (isDrawMov)
        {
            GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), movTexture);
            if (isShowMessage)
            {
                GUI.Label(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 20, 200, 40), "再点击一次退出动画播放！");
            }
        }
    }

    public void OnPlayButtonClick()
    {
        BlackMask._instance.Show();
        VSShow._instance.Show(hero1.spriteName, HeroName[UnityEngine.Random.Range(0, 9)]);
        StartCoroutine(LoadPlayScene());
    }

    IEnumerator LoadPlayScene()
    {
        yield return new WaitForSeconds(2);
        Application.LoadLevel(1);
    }

}
