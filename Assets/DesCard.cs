﻿using UnityEngine;
using System.Collections;

public class DesCard : MonoBehaviour {

    public float showTime = 4f;
    public static DesCard _instance;

    private UISprite sprite;
    private bool isShow = false;
    private float timer = 0;
    private UILabel harmLabel;
    private UILabel hpLabel;

    void Awake()
    {
        _instance = this;
        sprite = GetComponent<UISprite>();
        //gameObject.SetActive(false);
        sprite.alpha = 0;
        harmLabel = transform.Find("harmLabel").GetComponent<UILabel>();
        hpLabel = transform.Find("hpLabel").GetComponent<UILabel>();
    }

    void Update()
    {
        if (isShow)
        {
            timer += Time.deltaTime;
            if(timer > showTime)
            {
                sprite.alpha = 0;
                timer = 0;
                isShow = false;
            }
            else
            {
                sprite.alpha = (showTime - timer) / showTime;
            }
        }
    }

    public void ShowCard(string cardname)
    {
        gameObject.SetActive(true);
        sprite.spriteName = cardname;
        isShow = true;
        timer = 0;

        int harm, hp;
        InitProperty(out harm, out hp);
        harmLabel.text = harm + "";
        hpLabel.text = hp + "";
    }

    public void InitProperty(out int harm,  out int hp)
    {
        string extractProperties = sprite.spriteName;
        string[] singleProperties = extractProperties.Split('_');
        harm = int.Parse(singleProperties[1]);
        hp = int.Parse(singleProperties[2]);
    }
}
