﻿using UnityEngine;
using System.Collections;

public class Card : MonoBehaviour {
    public int harm;
    public int hp;
    public int needCrystal;

    private UISprite sprite;
    private UILabel hpLabel;
    private UILabel harmLabel;

    void Awake()
    {
        sprite = GetComponent<UISprite>();
        hpLabel = transform.Find("hpLabel").GetComponent<UILabel>();
        harmLabel = transform.Find("harmLabel").GetComponent<UILabel>();
    }

    private string cardName
    {
        get
        {
            return sprite.spriteName;
        }
    }

    void OnPress(bool isPressed)
    {
        if (isPressed)
        {
            DesCard._instance.ShowCard(cardName);
        }

    }

    public void SetDepth(int depth)
    {
        sprite.depth = depth;
        hpLabel.depth = depth + 1;
        harmLabel.depth = depth + 1;
    }

    public void ResetPos()
    {
        harmLabel.GetComponent<UIAnchor>().enabled = true;
        hpLabel.GetComponent<UIAnchor>().enabled = true;
    }

    public void ResetShow()
    {
        harmLabel.text = harm + "";
        hpLabel.text = harm + "";
    }

    public void InitProperty()
    {
        string extractProperties = sprite.spriteName;
        string[] singleProperties = extractProperties.Split('_');
        harm = int.Parse(singleProperties[1]);
        hp = int.Parse(singleProperties[2]);
        needCrystal = int.Parse(singleProperties[3]);
    }
}
